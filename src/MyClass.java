import java.util.Scanner;

public class MyClass {

	public static void main(String[] args) {
		
		
		Scanner scan = new Scanner(System.in);
		System.out.println("For which of the following do you want to calculate"
				+ " area and perimeter: circle, triangle, rectangle?");
		
		String figure = scan.nextLine();
		
			if(figure.equalsIgnoreCase("circle")){
				
				System.out.println("Please enter radius of the circle:");
				Scanner scan1 = new Scanner(System.in);
				double radius = scan1.nextDouble();
				
				Circle circle = new Circle(radius);						
				double area = circle.circleArea();
				System.out.println("The area of the circle is: " + area);
				double perimeter = circle.circlePerimeter();
				System.out.println("The perimeter of the circle is: " + perimeter);
				
			} else if (figure.equalsIgnoreCase("rectangle")){
				
				Scanner s2 = new Scanner(System.in);
				System.out.println("Please enter the height of the rectangle: ");
				double h = s2.nextDouble();
				System.out.println("Please enter the width of the rectangle: ");
				double w = s2.nextDouble();
				
				Rectangle rectangle = new Rectangle(h,w);
				double area = rectangle.rectangleArea();
				System.out.println("The area of the rectangle is: " + area);
				double perimeter = rectangle.rectanglePerimeter();
				System.out.println("The perimeter of the rectangle is: " + perimeter);
				
			}else if (figure.equalsIgnoreCase("triangle")){
				
				Scanner scan3 = new Scanner(System.in);
				System.out.println("Please enter the length of the first side: ");
				double a = scan3.nextDouble();
				System.out.println("Please enter the length of the second side: ");
				double b = scan3.nextDouble();
				System.out.println("Please enter the length of the third side: ");
				double c = scan3.nextDouble();
				
				Triangle triangle = new Triangle(a,b,c);
								
				double perimeter = triangle.trianglePerimeter();
				System.out.println("The perimeter of the triangle is: " + perimeter);
				double area = triangle.triangleArea();
				System.out.println("The area of the triangle is: " + area);		
			}
		}
	}

