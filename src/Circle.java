
public class Circle {
	public static double radius;
	
	public Circle(){
		radius=0.0;
	}
	public Circle(double r){
		radius = r;
	}
	
	public double circleArea(){
		return radius*radius*Math.PI;
	}
	
	public double circlePerimeter(){
		return 2*Math.PI*radius;
	}
	
	
}
