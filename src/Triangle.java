import java.lang.Math;

public class Triangle {
	public static double sideA;
	public static double sideB;
	public static double sideC;
	
	public Triangle(){
		sideA = 0.0;
		sideB = 0.0;
		sideC = 0.0;
	}
	
	public Triangle(double a, double b, double c){
		sideA = a;
		sideB = b;
		sideC = c;
	}
	
	public double trianglePerimeter(){
		return (sideA + sideB + sideC);
	}
	
	
	public double triangleArea(){
		double s = (sideA+sideB+sideC)/2;
		double root = Math.sqrt(s*(s-sideA)*(s-sideB)*(s-sideC));
		return 	root;
	}
}
