
public class Rectangle {
	public static double height;
	public static double width;
	
	public Rectangle(){
		height = 0.0;
		width = 0.0;
	}
	
	public Rectangle(double h, double w){
		height = h;
		width = w;
	}
	
	public double rectangleArea(){
		return height*width;
	}
	
	public double rectanglePerimeter(){
		return 2*(height+width);
	}
	
}
